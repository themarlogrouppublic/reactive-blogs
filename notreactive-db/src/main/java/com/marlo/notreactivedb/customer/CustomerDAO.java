package com.marlo.notreactivedb.customer;

import com.marlo.notreactivedb.customer.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories
public interface CustomerDAO extends JpaRepository<Customer, Integer> {
}
