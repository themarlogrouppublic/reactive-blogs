package com.marlo.reactivedb;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface CustomerDAO extends ReactiveCrudRepository<Customer, Integer> {
}
