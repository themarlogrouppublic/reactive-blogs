package com.marlo.reactivedb;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class ReactorTests {

    @Test
    public void testMono() {
        Mono<String> stringMono = Mono.just("Hey there, World");
        System.out.println(stringMono.subscribe().toString());
        stringMono.subscribe(s -> assertThat(s).isEqualTo("Hey there, World"));
        stringMono.subscribe(System.out::println);
        stringMono.subscribe(s -> doTheThingWithTheString(s));
    }

    @Test
    public void testFlux() {
        Flux<String> stringFlux = Flux.just("Hey", "there,", "World");
        stringFlux.subscribe(s -> assertThat("Hey there, World").contains(s));
        stringFlux.subscribe(System.out::println);

    }

    private void doTheThingWithTheString(String s) {
        System.out.println("doing that thing: " + s);

        var q = Map.of("apiid", "a", "q", "perth", "units", "metric");
    }
}
